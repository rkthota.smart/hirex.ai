import React from 'react';
import {
    Button, Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle,
} from 'reactstrap';

export default class MyJobs extends React.Component {
    constructor() {
        super();
        this.state = {
            getJobs: []
        }
    }

    componentDidMount() {
        let email = localStorage.getItem("useremail")
        let array = JSON.parse(localStorage.getItem("arrayData"));
        array.map((item, index) => {
            if (item.username == email) {
                console.log("..........")
                var newArray = array[index].userdata;
                this.setState({
                    getJobs: newArray
                })
            }

        })


    }


    handleHome = () => {
        this.props.history.push("/user")

    }
    handleItem = (item) => {
        var d1 = new Date(item.from);
        var d2 = new Date(item.to);
        return d1.toDateString()
    }


    handleItem2 = (item) => {
        var d2 = new Date(item.to);
        return d2.toDateString()
    }




    render() {
        return (
            <div>
                <div style={{ display: "flex" }}>
                    <h2>
                        Check all the Jobs Created
            </h2>
                    <Button
                        color="success"
                        style={{ marginLeft: "60%", float: "right" }}
                        onClick={this.handleHome}>
                        Go to Home
        </Button></div>
                <h3>
                    {this.state.getJobs.map((item, index) => {
                        return <div className="card-create"> <ul className="display">
                            <span style={{ color: "red" }}>   Job {index + 1}.</span>
                            <ol key={index}>
                                <div className="display">  <p style={{ marginRight: "12px" }}>Created </p>
                                    <span style={{ color: "#0000FF" }}> {this.handleItem(item)}</span>
                                    <p style={{ marginLeft: "12px", marginRight: "12px" }}     >Expires</p>
                                    <span style={{ color: "#0000FF" }}>{this.handleItem2(item)}</span>
                                    <p style={{ marginLeft: "12px", marginRight: "12px" }}     >Job</p>
                                    <span style={{ color: "#0000FF" }}>{item.job}</span>
                                    <p style={{ marginLeft: "12px", marginRight: "12px" }}     >Location</p>
                                    <span style={{ color: "#0000FF" }}>{item.location}</span>
                                    <p style={{ marginLeft: "12px", marginRight: "12px" }}     >Description</p>
                                    <span style={{ color: "#0000FF" }}>{item.description}</span>
                                    <p style={{ marginLeft: "12px", marginRight: "12px" }}     >Skills</p>
                                    <span style={{ color: "#0000FF" }}>{item.skills}</span>
                                </div>
                            </ol>
                        </ul></div>
                    })}
                </h3>


            </div>
        );
    }

}