import React from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem, NavLink, Card, CardImg, CardText, CardBody, } from 'reactstrap';

export default class User extends React.Component {
    constructor() {
        super();
        this.state = {
            collapsed: true,
            name: ""
        };
    }
    componentDidMount() {
        let name = localStorage.getItem("useremail");
        this.setState({
            name
        })
    }
    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }
    render() {
        return (
            <div>
                <div className="sidenav">

                    <Nav>
                        <NavItem>
                            <NavLink href="/myjobs">Created Jobs</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/newjob">Apply Job</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/">LogOut</NavLink>
                        </NavItem>
                    </Nav>
                    <hr />

                </div>
                <div class="row">
                    <div className="col-lg-5" />
                    <div className="col-lg-5">
                        <Card >

                            {/* <CardBody> */}
                            <h3>
                                logged in As: <span style={{ color: "red" }}>{this.state.name}</span>
                            </h3>
                            {/* </CardBody> */}
                        </Card>

                    </div> </div>
            </div>

        );
    }
}