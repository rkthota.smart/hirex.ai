import React, { Component, Fragment } from 'react';
import { Button, InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';
import DatePicker from 'react-date-picker';

var date = new Date();
export default class NewJob extends React.Component {
    constructor() {
        super();
        this.state = {
            fromDate: date,
            toDate: date,
            date: date,
            job: "",
            location: "",
            description: "",
            company: "",
            skills: "",
            open: "",
        }
        this.handleCancel = this.handleCancel.bind(this);
    }
    componentDidMount() {
        this.setState({
            fromDate: null,
            toDate: null,
        })
    }

    onToChange = (date) => {
        this.setState({
            toDate: date
        })

    }
    onFromChange = (date) => {
        this.setState({
            fromDate: date
        })

    }

    buttonChange = () => {
        var from = this.state.fromDate;
        var to = this.state.toDate;
        var job = this.state.job;
        var location = this.state.location;
        var description = this.state.description;
        var company = this.state.company;
        var skills = this.state.skills;
        var open = this.state.open;
        console.log("from", from, to, job, location, description, company, skills, open)
        if (from === null || to === null || job === "" || location === "" ||
            description === "" ||
            company === "" ||
            skills === "" ||
            open === ""

        ) {

            alert("All Fields are mandatory");
        }
        else {
            var obj = {};
            obj.from = from;
            obj.to = to;
            obj.flag = false;
            obj.job = job;
            obj.location = location;
            obj.description = description;
            obj.company = company;
            obj.skills = skills;
            obj.open = open;
            var email = localStorage.getItem("useremail")
            let arr = JSON.parse(localStorage.getItem("arrayData"));
            arr.map((item, index) => {
                if (item.username == email) {
                    var refArray = arr[index].userdata;
                    refArray.push(obj);
                }
            });
            localStorage.setItem("arrayData", JSON.stringify(arr));

            alert("Job applied successfully");
            this.props.history.push("/myjobs")
        }


    }
    handleCancel() {
        this.props.history.push("/user")
    }
    handleJob = (e) => {
        console.log("cje jfbkbvjb", e.target.name, e.target.value);
        let value = e.target.value;
        this.setState({
            [e.target.name]: value
        })
    }

    render() {
        return (
            <div>
                <h2 className="from-top-text" style={{marginLeft:"40%",marginBottom:"3%"}}>
                    {/* Apply For Job */}
                    Create New Job
                </h2>

                <div style={{ marginLeft: "2rem" }}>
                    <InputGroup size="lg" style={{ display: "flex", margin: "1rem" }}>
                        <InputGroupAddon addonType="prepend" className="input-job-create">Job Title</InputGroupAddon>
                        <Input className="two-job-create" style={{ width: "40%" }} name="job" onChange={this.handleJob} />
                    </InputGroup>
                    <br />
                    <InputGroup size="lg" style={{ display: "flex", margin: "1rem" }}>
                        <InputGroupAddon addonType="prepend" className="input-job-create">Job Description</InputGroupAddon>
                        <Input style={{ width: "40%" }} name="description" onChange={this.handleJob} />
                    </InputGroup>
                    <br />
                    <InputGroup size="lg" style={{ display: "flex", margin: "1rem" }}>
                        <InputGroupAddon addonType="prepend" className="input-job-create">Open positions</InputGroupAddon>
                        <Input style={{ width: "40%" }} name="open" onChange={this.handleJob} />
                    </InputGroup>
                    <InputGroup size="lg" style={{ display: "flex", margin: "1rem" }}>
                        <InputGroupAddon addonType="prepend" className="input-job-create">Location</InputGroupAddon>
                        <Input style={{ width: "40%" }} name="location" onChange={this.handleJob} />
                    </InputGroup>
                    <InputGroup size="lg" style={{ display: "flex", margin: "1rem" }}>
                        <InputGroupAddon addonType="prepend" className="input-job-create">Company Name</InputGroupAddon>
                        <Input style={{ width: "40%" }} name="company" onChange={this.handleJob} />
                    </InputGroup>
                    <InputGroup size="lg" style={{ display: "flex", margin: "1rem" }}>
                        <InputGroupAddon addonType="prepend" className="input-job-create">skills</InputGroupAddon>
                        <Input style={{ width: "40%" }} name="skills" onChange={this.handleJob} />
                    </InputGroup>

                </div>

                <div className="display-two">
                    <h5 style={{ paddingTop: "2rem" }}>
                        select Date :
                     </h5>
                    <div className="date1">

                        <div className="display">
                            <h5 className="from-text">  From:</h5>
                            <DatePicker
                                onChange={this.onFromChange}
                                value={this.state.fromDate}
                                dateFormat="DD/MM/YYYY"
                            />
                        </div>
                    </div>
                    <div className="date2">
                        <div className="display">
                            <h5 className="from-text">  To:</h5>
                            <DatePicker
                                onChange={this.onToChange}
                                value={this.state.toDate}
                            />
                        </div>
                    </div>
                </div>
                <div style={{ margin: "10px", marginLeft: "40%" }}>
                    <Button
                        color="warning"
                        onClick={this.handleCancel} className="date1">
                        Cancel
        </Button>
                    <Button
                        color="primary"
                        style={{ marginTop: "2rem", marginLeft: "5rem" }}
                        onClick={this.buttonChange}>
                        Apply
                </Button></div>
            </div>
        );
    }
}